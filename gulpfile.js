// Require

var gulp    = require('gulp'),
    gutil   = require('gulp-util'),
    sass    = require('gulp-sass'),
    concat  = require('gulp-concat'),
    prefix  = require('gulp-autoprefixer'),
    postcss = require('gulp-postcss'),
    rename  = require('gulp-rename'),
    uglify  = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps');
    cleanCSS = require('gulp-clean-css');
    tailwindcss = require('tailwindcss');

gulp.task('css', function () {
  return gulp.src(['src/styles/main.css'])
    .pipe(postcss([
      tailwindcss('tailwind.js'),
      require('autoprefixer'),
    ]))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('web/assets/css'));
});

gulp.task('scripts', function() {
  return gulp.src(['node_modules/fullpage.js/dist/fullpage.js', 'src/scripts/main.js'])
  .pipe(concat('main.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('web/assets/js'));
});

gulp.task('vendors', function() {
  return gulp.src(['node_modules/fullpage.js/vendors/easings.js', 'node_modules/fullpage.js/vendors/scrolloverflow.js'])
  .pipe(concat('vendor.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('web/assets/js'));
});

gulp.task('watch', function () {
  gulp.watch('src/styles/*.css', gulp.series('css'));
  gulp.watch('tailwind.js', gulp.series('css'));
  gulp.watch('src/scripts/*.js', gulp.series('scripts'));
});
