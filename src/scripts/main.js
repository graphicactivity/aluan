$(document).ready(function(){

    if($(window).innerWidth() >= 768) {
        $(document).ready(function() {
        	$('#fullpage').fullpage({
        		//options here
                //scrollBar: true,
                anchors:['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve'],
        		autoScrolling: true,
                scrollOverflow: true,
        		scrollHorizontally: false,
                normalScrollElements: '.stockists',
                lazyLoading: false
        	});

        	//methods
        	$.fn.fullpage.setAllowScrolling(true);
        });
    }

    // Start midnight
    $(document).ready(function(){
        // Change this to the correct selector for your nav.
        //$('header.fixed').midnight();
    });

    $('.menu').click(function() {
        $('.menuItems').show();
        $('.menuItems').removeClass('hidden');
        $('nav').addClass('h-screen');
        $('nav').removeClass('h-32');
        $(this).addClass('hidden');
        //$('.wordmark h1').removeClass('text-white');
        //$('.wordmark h1').addClass('text-coco');
    });

    $('.close, section, .menuItems li').click(function() {
        $('.menuItems').hide();
        $('.menuItems').addClass('hidden');
        $('nav').removeClass('h-screen');
        $('nav').addClass('h-32');
        $('.menu').removeClass('hidden');
        //$('.wordmark h1').addClass('text-white');
        //$('.wordmark h1').removeClass('text-coco');
    });

});
